package com.dbsms.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class HibernateConfiguration {
	
	@Autowired
	private DataSource dataSource;

	@Bean(name = "sessionFactory")
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
		localSessionFactoryBean.setPackagesToScan("com.dbsms");
		localSessionFactoryBean.setAnnotatedPackages("com.dbsms");
		localSessionFactoryBean.setDataSource(dataSource);
		localSessionFactoryBean.setHibernateProperties(new Properties() {
			private static final long serialVersionUID = 1L;
			// Set the Hibernate Properties here.
			{
				setProperty("hibernate.show_sql", "false");
				setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
			}
		});
		return localSessionFactoryBean;
	}

}
