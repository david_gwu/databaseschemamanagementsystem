package com.dbsms.model;

/**
 * Created by david on 12/10/15.
 */

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * CREATE TABLE "$$_DELTA_SQL_HIST"
 * (
 * CHANGEID     VARCHAR2 (20) NOT NULL ,
 * APPLIED_DATE TIMESTAMP ,
 * DB_USER      VARCHAR2 (15) ,
 * OS_USER      VARCHAR2 (15) ,
 * DELTA_AUTHOR VARCHAR2 (15) ,
 * COMMENTS     VARCHAR2 (1000) ,
 * TARGET       VARCHAR2 (30)
 * ) ;
 */
@Entity
@Table(name = "\"$$_DELTA_SQL_HIST\"")
public class DeltaSqlHist {

    @Id
    @Column(name = "CHANGEID")
    private String changeId;

    @Basic
    @Column(name = "APPLIED_DATE")
    private Date appliedDate;

    @Basic
    @Column(name = "DB_USER")
    private String dbUser;

    @Basic
    @Column(name = "OS_USER")
    private String osUser;

    @Basic
    @Column(name = "DELTA_AUTHOR")
    private String deltaAuthor;

    @Basic
    @Column(name = "COMMENTS")
    private String comments;

    @Basic
    @Column(name = "TARGET")
    private String target;

    @Transient
    private String deltaSQL;

    @Transient
    private String apply;


    public DeltaSqlHist() {
    }

    public DeltaSqlHist(String changeId, Date appliedDate, String dbUser, String osUser, String deltaAuthor,
                        String comments, String target, String deltaSQL, String apply) {
        this.changeId = changeId;
        this.appliedDate = appliedDate;
        this.dbUser = dbUser;
        this.osUser = osUser;
        this.deltaAuthor = deltaAuthor;
        this.comments = comments;
        this.target = target;
        this.deltaSQL = deltaSQL;
        this.apply = apply;
    }

    public String getChangeId() {
        return changeId;
    }

    public void setChangeId(String changeId) {
        this.changeId = changeId;
    }

    public Date getAppliedDate() {
        return appliedDate;
    }

    public void setAppliedDate(Date appliedDate) {
        this.appliedDate = appliedDate;
    }

    public String getDbUser() {
        return dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public String getOsUser() {
        return osUser;
    }

    public void setOsUser(String osUser) {
        this.osUser = osUser;
    }

    public String getDeltaAuthor() {
        return deltaAuthor;
    }

    public void setDeltaAuthor(String deltaAuthor) {
        this.deltaAuthor = deltaAuthor;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getDeltaSQL() {
        return deltaSQL;
    }

    public void setDeltaSQL(String deltaSQL) {
        this.deltaSQL = deltaSQL;
    }

    public String getApply() {
        return apply;
    }

    public void setApply(String apply) {
        this.apply = apply;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeltaSqlHist that = (DeltaSqlHist) o;

        if (!changeId.equals(that.changeId)) return false;
        if (appliedDate != null ? !appliedDate.equals(that.appliedDate) : that.appliedDate != null) return false;
        if (dbUser != null ? !dbUser.equals(that.dbUser) : that.dbUser != null) return false;
        if (osUser != null ? !osUser.equals(that.osUser) : that.osUser != null) return false;
        if (deltaAuthor != null ? !deltaAuthor.equals(that.deltaAuthor) : that.deltaAuthor != null) return false;
        if (comments != null ? !comments.equals(that.comments) : that.comments != null) return false;
        if (target != null ? !target.equals(that.target) : that.target != null) return false;
        if (deltaSQL != null ? !deltaSQL.equals(that.deltaSQL) : that.deltaSQL != null) return false;
        return !(apply != null ? !apply.equals(that.apply) : that.apply != null);

    }

    @Override
    public int hashCode() {
        int result = changeId.hashCode();
        result = 31 * result + (appliedDate != null ? appliedDate.hashCode() : 0);
        result = 31 * result + (dbUser != null ? dbUser.hashCode() : 0);
        result = 31 * result + (osUser != null ? osUser.hashCode() : 0);
        result = 31 * result + (deltaAuthor != null ? deltaAuthor.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        result = 31 * result + (target != null ? target.hashCode() : 0);
        result = 31 * result + (deltaSQL != null ? deltaSQL.hashCode() : 0);
        result = 31 * result + (apply != null ? apply.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DeltaSqlHist{" +
                "changeId='" + changeId + '\'' +
                ", appliedDate=" + appliedDate +
                ", dbUser='" + dbUser + '\'' +
                ", osUser='" + osUser + '\'' +
                ", deltaAuthor='" + deltaAuthor + '\'' +
                ", comments='" + comments + '\'' +
                ", target='" + target + '\'' +
                ", deltaSQL='" + deltaSQL + '\'' +
                ", apply='" + apply + '\'' +
                '}';
    }
}
