package com.dbsms.service.impl;

import com.dbsms.service.DeltaSqlHistFileService;
import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Created by david on 12/10/15.
 */
@Service
public class DeltaSqlHistFileServiceImpl implements DeltaSqlHistFileService {

    public static void main(String[] args) {
        DeltaSqlHistFileServiceImpl deltaSqlHistFileService = new DeltaSqlHistFileServiceImpl();
        deltaSqlHistFileService.appendToFile("\n-- Adding a another new line to the sql file and Push to Remote.\n", "Commit Message, test push to remote.");
    }

    @Override
    public void appendToFile(String sqlToAppend, String message) {
        try {
            FileUtils.write(new File("/home/ec2-user/deltasql/delta.sql"), sqlToAppend, Charset.defaultCharset(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }


        try (Git git = Git.open(new File("/home/ec2-user/deltasql/.git"))) {
            git.add()
                    .addFilepattern("delta.sql")
                    .call();

            git.commit()
                    .setMessage(message)
                    .call();

            CredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider("david_gwu","dG156Bla");
            git.push().setRemote("origin").setCredentialsProvider(credentialsProvider).call();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
